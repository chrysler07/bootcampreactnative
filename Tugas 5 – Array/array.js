console.log("\nNomor 1\n");

function Range(startNum, finishNum){
	let arr = [];
	if (startNum < finishNum) {
		for(let i = startNum; i <= finishNum; i++){
			arr.push(i);
		}
	}else if (startNum > finishNum) {
		for(let i = startNum; i >= finishNum; i--){
			arr.push(i);
		}
	}else if (startNum == null || finishNum == null) {
		arr.push(-1);
	}else if (startNum == null && finishNum == null) {
		arr.push(-1);
	}
	return arr;
}
console.log(Range(1, 10))
console.log(Range(1))
console.log(Range(11,18))
console.log(Range(54,50))
console.log(Range())

console.log("\nNomor 2\n");

function rangeWithStep(min, max, step){
	let arr = [];
	if (min < max) {
		for(let i = min; i <= max; i += step){
			arr.push(i);
		}
	}else if (min>max) {
		for(let i = min; i >= max; i -= step){
			arr.push(i);
		}
	}
	return arr;
}
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

console.log("\nNomor 3\n");

function Range(startNum, finishNum, step){
	let arr = [];
	if (step == null) {
		step = 1
	}
	if (startNum < finishNum ) {
		for(let i = startNum; i <= finishNum; i+=step ){
			arr.push(i);
		}
	}else if (startNum > finishNum) {
		for(let i = startNum; i >= finishNum; i-=step ){
			arr.push(i);
		}
	}else if (startNum /= finishNum == step) {
		arr.push(1);
	}

	return arr;
}
function sum(arr){  
    var total = 0; // need to create a variable outside the loop scope
    for(var i in arr){  
    	total = total+arr[i];  
    }
    return total;
}  
console.log(sum(Range(1,10,2))); // pass the array correctly 
console.log(sum(Range(1,10))) // 55
console.log(sum(Range(5, 50, 2))) // 621
console.log(sum(Range(15,10))) // 75
console.log(sum(Range(20, 10, 2))) // 90
console.log(sum(Range(1))) // 1
console.log(sum(Range())) // 0 

console.log("\nNomor 4\n");

var input = [
["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

function dataHandling(a){
	for (var i = 0; i < input.length; i++) {
		console.log('Nomor ID: ' + input[i][0])
		console.log('Nama Lengkap: ' + input[i][1])
		console.log('TTL: ' + input[i][2] + ' ' +input[i][3])
		console.log('Hobi: ' + input[i][4])
		console.log()
	}
}

dataHandling(input)

console.log("\nNomor 5\n");

function balikKata(str) {
	var currentString = str;
	var newString = '';
	for (let i = str.length - 1; i >= 0; i--) {
		newString = newString + currentString[i];
	}

	return newString;
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log("\nNomor 6\n");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input){
	input.splice(1, 1);
	input.splice(2, 0, "Roman Alamsyah Elsharawy");
	input.splice(1, 1);
	input.splice(2, 0, "Provinsi Bandar Lampung");
	input.splice(4, 1);
	input.splice(5, 0, "Pria");
	input.splice(6, 0, "SMA Internasional Metro ");
	console.log(input);

	console.log();

	var x = input[3].split("/");

	var salinX = x.slice();

	if (x[1].charAt(0) == 0) {
		x[1] = x[1].charAt(1);
	};

	var tampung = x[1];

	switch(parseInt(tampung)){
		case 1: {console.log("Januari"); break; }
		case 2: {console.log("Februari"); break; }
		case 3: {console.log("Maret"); break; }
		case 4: {console.log("April"); break; }
		case 5: {console.log("Mei"); break; }
		case 6: {console.log("Juni"); break; }
		case 7: {console.log("Juli"); break; }
		case 8: {console.log("Agustus"); break; }
		case 9: {console.log("September"); break; }
		case 10: {console.log("Oktober"); break; }
		case 11: {console.log("November"); break; }
		case 12: {console.log("Desember"); break; }
		default:  { console.log('Salah'); break;}
	}

	console.log();
	x.sort(function (value1, value2) { return value2 - value1 } ) ;
	console.log(x); 

	console.log();
	var join = salinX.join("-");
	console.log(join);

	console.log();
	var nama = input[1];
	var irisan = nama.slice(0, 15);
	console.log(irisan);

}

dataHandling2(input);